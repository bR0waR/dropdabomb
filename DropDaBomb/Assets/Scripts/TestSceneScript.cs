﻿using UnityEngine;
using System.Collections;

public class TestSceneScript : MonoBehaviour {

	public KeyCode trigger;

    public float duration = 1;

    private float currentDuration;

	// Use this for initialization
	void Start () 
    {
        currentDuration = 0;
	}
	
	// Update is called once per frame
	void Update () 
    {
		if (Input.GetKey(trigger) && currentDuration <= 0)
        {
            testMethod();	
            currentDuration = duration;
        } 
        else if (currentDuration > 0)
        {
            currentDuration -= Time.deltaTime;
        }
	}

	void testMethod() 
	{
		Debug.Log("Working!!");
	}
}
